package com.stay4it.rxjava;

import java.util.concurrent.TimeUnit;


import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * RxJava Backpressure（背压）
 * 参考链接：http://blog.csdn.net/jdsjlzx/article/details/51868640
 */
public class RxJava15 {

	/**
	 * 什么情况下出现MissingBackpressureException 
	 * 抛出MissingBackpressureException往往就是因为，
	 * 被观察者发送事件的速度太快，而观察者处理太慢，而且你还没有做相应措施，所以报异常。
	 * 
	 * test1()方法中，被观察者发送事件的速度是观察者处理速度的1000倍
	 * 
	 * @throws InterruptedException 
	 */
	private static void test1() throws InterruptedException {
		
		Observable.interval(1, TimeUnit.MILLISECONDS)
        //将观察者的工作放在新线程环境中
        .observeOn(Schedulers.newThread())
        //观察者处理每1000ms才处理一个事件
        .subscribe(new Subscriber<Long>() {
            @Override
            public void onCompleted() {
                System.out.println("onCompleted");
            }
            @Override
            public void onError(Throwable e) {
            	System.out.println("onError ："+ e);
            }
            @Override
            public void onNext(Long value) {
            	
            	try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            	System.out.println("onNext value :"+ value);
            }
        });
		
		Thread.sleep(2000); 
		
	}
	
	
	public static void main(String[] args) throws InterruptedException {
		test1();

	}

}
