package com.stay4it.rxjava;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.functions.Func0;
import rx.schedulers.Schedulers;

public class RxJava03 {
	
	public static Observable<Long> DeferObservable() {
		return Observable.defer(new Func0<Observable<Long>>() {

			@Override
			public Observable<Long> call() {
				return Observable.just(System.currentTimeMillis());
			}
			
		});
    }

	public static Observable<Long> JustObservable() {
        return Observable.just(System.currentTimeMillis());
    }
	
	public static Observable<Long> deferObservable =  DeferObservable();
	public static Observable<Long> justObservable =  JustObservable();

	/**
	 * Range
	 * Range操作符根据输入的初始值n和数目m发射一系列大于等于n的m个值
	 */
	private static void test1(){
		
		Observable.range(10, 5)
		.subscribe(new Subscriber<Integer>() {

			@Override
			public void onCompleted() {
				System.out.println("onCompleted");		
			}
			
			@Override
			public void onNext(Integer value) {
				System.out.println("onSuccess value = " + value);
			}

			@Override
			public void onError(Throwable error) {
				System.out.println("onError error = " + error);
			}
		});
		
	}
	
	/**
	 * Defer
	 * Defer操作符只有当有Subscriber来订阅的时候才会创建一个新的Observable对象,
	 */
	private static void test2(){
		
		Observable.defer(new Func0<Observable<Long>>() {

			 //注意此处的call方法没有Subscriber参数,而create操作符有Subscriber参数
			@Override
			public Observable<Long> call() {
				return Observable.just(System.currentTimeMillis());
			}
		})
		.subscribe(new Subscriber<Long>() {
			
			@Override
			public void onCompleted() {
				System.out.println("onCompleted");		
			}
			
			@Override
			public void onNext(Long value) {
				System.out.println("onSuccess value = " + value);
			}
			
			@Override
			public void onError(Throwable error) {
				System.out.println("onError error = " + error);
			}
		});
		
	}
	/**
	 * Just
	 * Just操作符将某个对象转化为Observable对象，并且将其发射出去，可以使一个数字、一个字符串、数组、Iterate对象等，是一种非常快捷的创建Observable对象的方法，在以后的例子里会大量使用。
	 */
	private static void test3(){
		
		Observable.just(System.currentTimeMillis())
		.subscribe(new Subscriber<Long>() {
			
			@Override
			public void onCompleted() {
				System.out.println("onCompleted");		
			}
			
			@Override
			public void onNext(Long value) {
				System.out.println("onSuccess value = " + value);
			}
			
			@Override
			public void onError(Throwable error) {
				System.out.println("onError error = " + error);
			}
		});
		
	}
	
	/**
	 * Defer、Just区别
	 * Defer:每次订阅都会得到一个刚创建的最新的Observable对象，这可以确保Observable对象里的数据是最新的;
	 * Just:每次订阅不会创建的新的Observable对象
	 */
	private static void test4(){
		
		deferObservable.subscribe(new Observer<Long>() {
			
			@Override
			public void onCompleted() {
				System.out.println("onCompleted");		
			}
			
			@Override
			public void onNext(Long value) {
				SimpleDateFormat  sdf =new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
				System.out.println("defer onSuccess value = " + sdf.format(new Date(value)));
			}
			
			@Override
			public void onError(Throwable error) {
				System.out.println("onError error = " + error);
			}
		});
		
		justObservable.subscribe(new Observer<Long>() {
			
			@Override
			public void onCompleted() {
				System.out.println("onCompleted");		
			}
			
			@Override
			public void onNext(Long value) {
				SimpleDateFormat  sdf =new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
				System.out.println("just onSuccess value = " + sdf.format(new Date(value)));
			}
			
			@Override
			public void onError(Throwable error) {
				System.out.println("onError error = " + error);
			}
		});
		
	}
	
	/**
	 * Interval
	 * Interval所创建的Observable对象会从0开始，每隔固定的时间(long interval)发射一个数字。
	 * 需要注意的是这个对象是运行在computation Scheduler,所以如果需要在view中显示结果，要在主线程中订阅。
	 * http://blog.csdn.net/jdsjlzx/article/details/52860632
	 */
	private static void test5(){
		
		Observable.interval(1, TimeUnit.SECONDS,Schedulers.immediate())
		.subscribe(new Subscriber<Long>() {
			
			@Override
			public void onCompleted() {
				System.out.println("onCompleted");		
			}
			
			@Override
			public void onNext(Long value) {
				SimpleDateFormat  sdf =new SimpleDateFormat("HH时mm分ss秒");
				System.out.println("onSuccess value = "+ value +  ", 现在时间：" + sdf.format(new Date()));
			}
			
			@Override
			public void onError(Throwable error) {
				System.out.println("onError error = " + error);
			}
		});
		
	}
	
	public static void main(String[] args) throws InterruptedException {
		/*test4();
		System.out.println("休息2秒");
		Thread.sleep(2000);
		test4();*/
		
		
		test1();
		
	}
	
	
}
