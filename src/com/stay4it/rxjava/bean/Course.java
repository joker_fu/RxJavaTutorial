package com.stay4it.rxjava.bean;

public class Course {

	public String name;
	public int id;
	
	public Course(String name, int id) {
		super();
		this.name = name;
		this.id = id;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	
	
}
