package com.stay4it.rxjava.bean;

public class Entity {

	public Course course;
	public Student student;
	public Entity(Course course, Student student) {
		super();
		this.course = course;
		this.student = student;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return student + " 喜欢的课程是：" + course;
	}
}
