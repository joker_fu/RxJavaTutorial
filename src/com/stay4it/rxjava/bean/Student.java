package com.stay4it.rxjava.bean;

public class Student {
	public String name;
	public int age;
	
	public Student(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + "]";
	}
	
	 
	
	/*@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
        if (obj == null || getClass() != obj.getClass()) {
        	return false;
        }
        
		Student student = (Student) obj;

        return age == student.age;
	}*/
	

}
