package com.stay4it.rxjava;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observable.OnSubscribe;
import rx.Observer;
import rx.Scheduler;
import rx.Subscriber;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.internal.operators.OnSubscribeAutoConnect;
import rx.schedulers.Schedulers;

public class RxJava01 {

	static List<String> list = Arrays.asList("小王","小李","赵四");
	
	public static void main(String[] args) {
		test0();
	}
	
	private static void test0(){
		Observable.just(null);
	}
	
	private static void test1(){
		Observable<String> observable = Observable.create(new OnSubscribe<String>(){

			@Override
			public void call(Subscriber<? super String> t) {
				// TODO Auto-generated method stub
				
				int n = 1/0;
				
				t.onNext("a");
				t.onNext("b");
				t.onNext("c");
				t.onCompleted();
				
			}
			
		});
		
		Observer<String> observer = new Observer<String>() {

			@Override
			public void onCompleted() {
				// TODO Auto-generated method stub
				System.out.println("onCompleted ");
			}

			@Override
			public void onError(Throwable e) {
				// TODO Auto-generated method stub
				System.out.println("onError " + e.toString());
			}

			@Override
			public void onNext(String t) {
				// TODO Auto-generated method stub
				System.out.println("onNext t = " + t);
			}
		};
		
		observable.subscribe(observer);
		
	}
	
	private static void test2(){
		Observable.from(list).subscribe(new Observer<String>() {

			@Override
			public void onCompleted() {
				// TODO Auto-generated method stub
				System.out.println("onCompleted ");
			}

			@Override
			public void onError(Throwable e) {
				// TODO Auto-generated method stub
				System.out.println("onError " + e.toString());
			}

			@Override
			public void onNext(String t) {
				// TODO Auto-generated method stub
				System.out.println("onNext t = " + t);
			}
		});
		
	}
	
	private static void test3(){
		Observable.from(list).subscribe(new Subscriber<String>() {

			@Override
			public void onCompleted() {
				// TODO Auto-generated method stub
				System.out.println("onCompleted ");
			}

			@Override
			public void onError(Throwable e) {
				// TODO Auto-generated method stub
				System.out.println("onError " + e.toString());
			}

			@Override
			public void onNext(String t) {
				// TODO Auto-generated method stub
				System.out.println("onNext t = " + t);
			}
		});
		
	}
	
	private static void test4(int lenth){
		List<Integer> list = new ArrayList<>();
		int[] array = new int[lenth];
		for (int i = 0; i < lenth; i++) {
			list.add(10+i);
		}
		
		for (int i = 0; i < lenth; i++) {
			Random random = new Random();
			int pos = random.nextInt(list.size());
			array[i] = list.remove(pos);
			System.out.println("array["+i+"] = " + array[i]);
		}
		
	}

}
